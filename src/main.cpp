#include <iostream>

#include "../include/chip.hpp"
#include "../include/SDL.hpp"


int
main(int argc, char** argv)
{
	/* If no rom is given */
	if( !argv[1] )
	{
		std::cout << "[INFO] Use: ./out dir/to/rom {-debug}" << std::endl;
		return EXIT_FAILURE;
	}

	Chip8* emu = new Chip8(argv[1]);
	SDL* sdl = new SDL();

	if ( argv[2] && strcmp ( argv[2], "-debug" ) == 0 )
	{
		emu->debugFlag = true;
	}

	/* Init SDL */
	sdl->initsdl();

	while(!sdl->quit)
	{
		if(SDL_PollEvent(&sdl->event))
		{
			switch(sdl->event.type)
			{
				case SDL_KEYDOWN:
				case SDL_KEYUP:
					sdl->key_press(emu->key);
			}
		}
		emu->cycle();

		sdl->render(emu->gfx);
		
		if ( emu->soundTimer > 0 )
		{
			sdl->playbeep(emu->soundTimer);
			emu->soundTimer = 0;
		}
	}
	delete [] sdl, emu; /* Delete class objs from memory and call deconstructors */

	return EXIT_SUCCESS;
}
