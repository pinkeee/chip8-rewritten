#include "../include/chip.hpp"

void
Chip8::initemu(void)
{	
	this->pc = 0x0200; /* Reset program counter to 0x0200 (512) */
	this->I = 0x0000; /* Reset CPU index reg */
	this->sp = 0x0000; /* Reset stack pointer */
	this->delayTimer = 0x0000; /* Reset delay timer */
	this->soundTimer = 0x0000; /* Reset sound timer */

	/* These are the characters that the ROM we load in will use to draw stuff to the screen */
	std::array<unsigned char,80> fontset =
	{ 
		0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
		0x20, 0x60, 0x20, 0x20, 0x70, // 1
		0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
		0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
		0x90, 0x90, 0xF0, 0x10, 0x10, // 4
		0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
		0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
		0xF0, 0x10, 0x20, 0x40, 0x40, // 7
		0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
		0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
		0xF0, 0x90, 0xF0, 0x90, 0x90, // A
		0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
		0xF0, 0x80, 0x80, 0x80, 0xF0, // C
		0xE0, 0x90, 0x90, 0x90, 0xE0, // D
		0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
		0xF0, 0x80, 0xF0, 0x80, 0x80  // F
	};
		
	/* Iterate through the array and add it to memory */
	for ( auto i = 0; i < fontset.size(); ++i)
	{
		this->memory[i] = fontset[i];
	}
}

void
Chip8::loadRom(std::string ROM)
{
	/* Open ifstream of ROM file */
	std::ifstream romFile(ROM, std::ios::in | std::ios::binary | std::ios::ate);

	if (!romFile)
	{
		std::cout << "\n[ERROR] Error opening rom file, closing..." << std::endl;
		std::exit(0);
	}
	/* If we made it through that we assume that the rom is open and ready to be loaded into ram. */
	
	/* Find size of ROM */
	romFile.seekg(0, std::ios::end);
	const auto size = romFile.tellg();
	romFile.seekg(0, std::ios::beg);

	/* We do total size of memory vector minus the amount of memory the fonts will take up. That is then the free space we have left to load stuff into memory. */
	if(size < (this->memory.size() - 0x0200))
	{
        /* read ROM into memory array at the address 0x0200 */
        romFile.read((char*)&this->memory[0x0200],size);
        
		std::cout << "[INFO] ROM loaded successfully!" << std::endl;
        std::cout << "[INFO] Loaded " << size << " bytes into memory." << std::endl;

        romFile.close();
		return;
	}
	std::cout << "[ERROR] Error loading ROM ' " << ROM << " '. Memory size is " << this->memory.size() << " and ROM size is " << size << " . Exiting..." << std::endl;
	romFile.close();
	
	std::exit(0);
}

void
Chip8::cycle(void)
{
	const auto opcode = this->memory[this->pc] << 8 | this->memory[this->pc + 1];

	const auto currentOpcode = opcode & 0xF000;

	const auto n = opcode & 0x000F;
	const auto nn = opcode & 0x00FF;
	const auto nnn = opcode & 0x0FFF;

	unsigned char* VX = &V[(opcode & 0x0F00) >> 8]; /* VX equals  to v at index opcode shifted to right 8 bits */
	unsigned char* VY = &V[(opcode & 0x00F0) >> 4]; /* VY same as VX but shifted 4 bits */

	if (this->debugFlag)
	{
		std::cout << "\n _______________ " << std::endl;
		std::cout << std::hex << std::setfill('0') << std::setw(2) << "Current instruction: 0x" << opcode << std::endl;
		std::cout << std::hex << std::setfill('0') << std::setw(2) << "Current opcode: 0x" << currentOpcode << std::endl;
		std::cout << "PC is: 0x" << this->pc << std::endl;
		std::cout << "NNN: " << nnn << std::endl;
		std::cout << "\n _______________ " << std::endl;
	}

	switch(currentOpcode)
	{
		case 0x0000:
		{
			switch(nnn)
			{
				/* 0x00E0 clears the screen */
				case 0x00E0:
				{
					for ( auto i = 0; i < W; ++i )
					{
						for ( auto j = 0; j < H; j++ )
						{
							/* Set all of graphics buf to 0, meaning nothing is rendered */
							this->gfx[i][j]=0;
						}
					}	
					this->pc += 2;
					break;
				}

				/* Return from a subroutine. We do this by going back to the last instuction in stack.
					So all we need todo is minus 1 from the stack pointer and set the pc to it so in the 
					next cycle we can exec it. */
				case 0x00EE: { this->sp--; this->pc = this->stack[this->sp]; this->pc += 2; break; }

				default: { std::cout << "[ERROR] Unknown opcode 0x" << opcode << std::endl; break; }
			}
		break;
		}
		
		/* Jump to address NNN by setting the current val of NNN to the PC (program counter) */
		case 0x1000: { this->pc = nnn; break; }

		/* Call subroutine at NNN by setting the next sp address in stack to the PC, then setting the PC to NNN */
		case 0x2000: { this->stack[this->sp] = this->pc; this->sp++; this->pc = nnn; break; }

		/* Skip next instruction if VX (x) is equal to NN.
		   We do this by using a nice little ternary op, if
		   x does equal nn then we plus two to the pc else we dont 
		   plus any as 2 is already added to it at the bottom of this whole
		   switch statement. */
		case 0x3000: { this->pc += (*VX == nn) ? 4 : 2; break; }
		
		/* Skip next instruction if VX (x) does not equal nn. (Same as above pretty much). */
		case 0x4000: { this->pc += (*VX != nn) ? 4 : 2; break; }

		/* Skip next instruction if VX (x) does equal YX (y). (Same as above pretty much). */
		case 0x5000: { this->pc += (*VX == *VY) ? 4 : 2; break; }

		/* Set YX (x) equal to nn */
		case 0x6000: { *VX = nn; this->pc += 2; break; }
			
		/* Add nn to YX (x) */
		case 0x7000: { *VX += nn; this->pc += 2; break; }

		case 0x8000: 
			/* Use another switch as we have many 0x8000s */
			switch(n)
			{
				/* Sets VX (x) to the value of VY (y) */
				case 0x0000: { *VX = *VY; this->pc += 2; break; }

				/* Sets VX (x) to VX (x) OR (Bitwise OR operator) VY (y) */
				case 0x0001: { *VX = *VX | *VY; this->pc += 2; break; }

				/* Sets VX (x) to VX (x) AND (Bitwise AND operator) VY (y) */
				case 0x0002: { *VX = *VX & *VY; this->pc += 2; break; }

				/* Sets VX (x) to VX (x) XOR (Bitwise XOR operator) VY (y) */
				case 0x0003: { *VX = *VX ^ *VY; this->pc += 2; break; }

				/* Adds VY (y) to VX (x). VF (v) is set to 1 when there's a carry, and to 0 when there is not. */
				case 0x0004: { this->V[0xF] = ((0xFF - *VX) < *VY) ? 1 : 0; *VX += *VY; this->pc += 2; break; }

				/* VY is subtracted from VX. VF is set to 0 when there's a borrow, and 1 when there is not. */
				case 0x0005: { this->V[0xF] = (*VX < *VY) ? 1 : 0; *VX -= *VY; this->pc += 2; break; }

				/* Stores the least significant bit of VX in VF and then shifts VX to the right by 1. */
				case 0x0006: { this->V[0xF] = *VX & 0x0001; *VX >>= 1; this->pc += 2; break; }

				/* Sets VX to VY minus VX. VF is set to 0 when there's a borrow, and 1 when there is not. */
				case 0x0007: { this->V[0xF] = (*VX <= *VY) ? 1 : 0; *VX -= *VY - *VX; this->pc += 2; break; }

				/* Stores the most significant bit of VX in VF and then shifts VX to the left by 1. */
				case 0x000E: { this->V[0xF] = (*VX & 0x80) >> 7; *VX <<= 1; this->pc += 2; break; }

				default: { std::cout << "[ERROR] Unknown opcode 0x" << opcode << std::endl; break; }
			}
			break;
		
		/* Skips the next instruction if VX does not equal VY. */
		case 0x9000: { this->pc += (*VX != *VY) ? 4 : 2; break; }

		/* Sets I to the address NNN. */
		case 0xA000: { this->I = nnn; this->pc += 2; break; }

		/* Jumps to the address NNN plus V0. */
		case 0xB000: { this->pc = this->V[0x0] + nnn; break; }
		
		/* Set VX (x) to the result of a bitwise operation on a random number (0-255) and NN (nn) */
		case 0xC000: 
		{ 
			// seed gen
			std::random_device seeder; 
			std::ranlux48 gen(seeder()); 
			// set limits from 0-255
			std::uniform_int_distribution<int16_t> uniform_0_255(0, 255); 

			// set x to the random number by calling the func, bitwise AND on nnn
			*VX = (uniform_0_255(gen) & nnn);

			this->pc += 2;
			break;
		}

		/* Draws a sprite at coordinate (VX, VY) that has a width of 8 pixels and a height of N pixels */
		case 0xD000:
		{
				this->V[0xF] = 0; /* Reset CPU flag */

				for ( auto i = 0; i < n; ++i )
				{
					auto drawPixels = this->memory[(this->I + i)];

					for ( auto j = 0; j < SCALE; ++j )
					{
						if ( (drawPixels & (0x80 >> j) ) != 0 )
						{
							if ( this->gfx[*VX + j][*VY + i] == 1 )
							{
								this->V[0xF] = 1;
							}
							this->gfx[*VX + j][*VY + i] ^= 1;
						}
					}	
				}
				this->pc += 2;
				break;
		}
	 
		case 0xE000:
		{
			switch(nn)
			{
				/* Skips the next instruction if the key stored in VX is pressed. */
				case 0x009E: { this->pc += ( this->key[*VX] == 1 ) ? 4 : 2; break; }

				/* Skips the next instruction if the key stored in VX is not pressed. */
				case 0x00A1: { this->pc += ( this->key[*VX] == 0 ) ? 4 : 2; break; }

				default: { std::cout << "[ERROR] Unknown opcode 0x" << opcode << std::endl; break; }
			}
			break;
		}

		case 0xF000:
		{
			switch(nn)
			{
				/* Delay emu 20milliseconds, set VX to the value of the delay timer. */
				case 0x0007: { *VX = this->delayTimer; this->pc += 2; break; }

				/* A key press is awaited, and then stored in VX. (Blocking Operation. All instruction halted until next key event) */
				case 0x000A: { *VX = this->waitkey(this->key); this->pc += 2; break; }

				/* Sets the delay timer to VX. */
				case 0x0015: { this->delayTimer = *VX; this->timeAtWrite = std::chrono::high_resolution_clock::now(); this->pc += 2; break; }

				/* Sets the sound timer to VX. */
				case 0x0018: { this->soundTimer = *VX; this->pc += 2; break; }

				/* Adds VX to I. VF is not affected. */
				case 0x001E: { this->I += *VX; this->pc += 2; break; }

				/* Sets I to the location of the sprite for the character in VX. */
				case 0x0029: { this->I = *VX * 5; this->pc += 2; break; } /* sprites are stored away from x */

				/* Stores the binary-coded decimal representation of VX, with the most significant of three digits at the address in I, the middle digit at I plus 1, and the least significant digit at I plus 2. 
				   In other words, take the decimal representation of VX, place the hundreds digit in memory at location in I, the tens digit at location I+1, and the ones digit at location I+2. */
				case 0x0033: 
				{
					this->memory[this->I] = ((int)*VX / 100);  
					this->memory[this->I] = ((int)*VX % 100 / 10);
					this->memory[this->I] = ((int)*VX % 10);

					this->pc += 2;
					break;
				}

				/* Stores V0 to VX (including VX) in memory starting at address I. */
				case 0x0055: 
				{ 
					auto offset = ((opcode & 0x0F00) >> 8);

					for ( auto i = 0; i < offset + 1; ++i )
					{
						this->memory[this->I + i] = this->V[i];
					}
					this->pc += 2;
					break;
				}

				/* Fills V0 to VX (including VX) with values from memory starting at address I. */
				case 0x0065:
				{
					auto offset = ((opcode & 0x0F00) >> 8);

					for ( auto i = 0; i < offset + 1; ++i )
					{
						this->V[i] = this->memory[this->I + i];
					}
					this->pc += 2;
					break;
				}
				default: { std::cout << "[ERROR] Unknown opcode 0x" << opcode << std::endl; break; }
			}
			break;

		}
	}
	/* Call clock function */
	this->clocktick();
}

void
Chip8::clocktick()
{
	if ( this->delayTimer == 0 ) { return; }

	const auto timeNow = std::chrono::high_resolution_clock::now();
	if (std::chrono::duration_cast<std::chrono::milliseconds>(timeNow - this->timeAtWrite)
        .count() >= 1000 / 60) {
    --delayTimer;
    this->timeAtWrite = timeNow;
  	}
}

/* Constructor */
Chip8::Chip8(std::string romName)
{
	/* Here we will call functions that are can just be called when we make an inst of our class */
	this->initemu(); /* Init main emu vars */
	this->loadRom(romName); /* Load ROM into correct memory pos */
}