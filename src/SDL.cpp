#include "../include/SDL.hpp"

void
SDL::initsdl(void)
{
	/* For whatever reason initing sdl fails, remove everything from memory and die */
	if(SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		std::cout << SDL_GetError() << std::endl;
		this->destroy();
	}
	
	this->window = SDL_CreateWindow("Chip8 Emulator",
									SDL_WINDOWPOS_CENTERED,
									SDL_WINDOWPOS_CENTERED,
									W * SCALE, 
									H *SCALE,
									SDL_WINDOW_SHOWN | SDL_WINDOW_VULKAN
	);

	if(!this->window)
	{
		std::cout << SDL_GetError() << std::endl;
		this->destroy();
	}

	/* Set up surface */
	this->surface = SDL_GetWindowSurface(this->window);

	/* Init audio function, set up audio vars */
	Mix_OpenAudio(22050, AUDIO_S16SYS, 2, 640);

	/* Load audio file */
	this->audioInterface = Mix_LoadWAV("beep.ogg");
	if(!this->audioInterface)
	{
		std::cout << SDL_GetError() << std::endl;
		this->destroy();
	}
	
	/* Set up rgbBuffs */
	this->rgbBuff[0] = SDL_MapRGB(this->surface->format, 0x00, 0x00, 0x00);
	this->rgbBuff[1] = SDL_MapRGB(this->surface->format, 0xFF, 0xFF, 0xFF);

	/* Set quit to false as the main emu loop depends on this being false */
	this->quit = false;
}

int
SDL::key_press(unsigned char key[16])
{	
	// so, here we are going to check the event and check every valid key to see if one is true, if so
	// we set the corrosponding place in the array 'key' to kcode. (not the best explanation i know)
	int kcode;

	switch(this->event.key.keysym.sym)
	{
		case SDLK_x:
			kcode = 0x0;

			break;
		case SDLK_1:
			kcode = 0x1;

			break;
		case SDLK_2:
			kcode = 0x2;

			break;
		case SDLK_3:
			kcode = 0x3;

			break;
		case SDLK_q:
			kcode = 0x4;

			break;
		case SDLK_w:
			kcode = 0x5;

			break;
		case SDLK_e:
			kcode = 0x6;

			break;
		case SDLK_a:
			kcode = 0x7;

			break;
		case SDLK_s:
			kcode = 0x8;

			break;
		case SDLK_d:
			kcode = 0x9;

			break;
		case SDLK_z:
			kcode = 0xA;

			break;
		case SDLK_c:
			kcode = 0xB;

			break;
		case SDLK_4:
			kcode = 0xC;

			break;
		case SDLK_r:
			kcode = 0xD;

			break;
		case SDLK_f:
			kcode = 0xE;

			break;
		case SDLK_v:
			kcode = 0xF;

			break;
		case SDLK_ESCAPE:
			this->destroy();
			break;
		default:
			return -1;
	}	
	// we grab key from inside the class
	key[kcode] = (this->event.type == SDL_KEYDOWN); 

	return kcode;
}

int
SDL::waitkey(unsigned char key[16])
{
	std::cout << "[INFO] AWAITING KEYPRESS" << std::endl;

	while(SDL_WaitEvent(&this->event))
	{
		switch(this->event.type)
		{
			case SDL_KEYUP:
			case SDL_KEYDOWN:
				int i=key_press(key);
				
				if(i != -1)
				{					
					return i;
				}
			break;
		}
	}
	return EXIT_SUCCESS;
}

void
SDL::destroy(void)
{
	// we need to keep this function as SDL does not have some sort of close function
	// When we want to exit the emulator we need to call this function.
	std::cout << "[INFO] Destroying SDL" << std::endl;

	/* Close audio buff */
	Mix_FreeChunk(this->audioInterface);
	Mix_CloseAudio();

	/* Close window bufs (remove from mem) */
	SDL_DestroyWindow(this->window);
	SDL_Quit();

	std::cout << "[INFO] Dying, goodbye." << std::endl;

	std::exit(EXIT_FAILURE);
}

void
SDL::render(unsigned char gfx[64][32])
{
	for ( auto i = 0; i < W; ++i )
	{
		colourBuff.x = i * SCALE;
		
		for ( auto j = 0; j < H; ++j )
		{
			colourBuff.y = j * SCALE;
			
			// if we already have a pixel there dont bother rendering it, waste of time and resources
			if ( gfx[i][j] == 1 && this->currentGfx[i][j] == 1 )
			{
				/* We dont need to bother rendering it */
				continue;
			} else {
				/* Pixel has changed so lets render it */
				SDL_FillRect(this->surface, &this->colourBuff, this->rgbBuff[gfx[i][j]]);
			}
			this->currentGfx[i][j] = gfx[i][j];
		}
	}
	SDL_UpdateWindowSurface(this->window);
}

void
SDL::playbeep(uint8_t soundTimer)
{
    Mix_PlayingMusic() == 0 ?  Mix_PlayChannel(-1, this->audioInterface , 0) : Mix_HaltMusic();
}

/* Deconstructor */
SDL::~SDL()
{
	this->destroy();
}