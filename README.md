# chip8-rewritten
A basic Chip8 emulator written in C++ using the SDL2 library. This emulator uses the Vulkan graphics API through SDL for that butter smooth gameplay :)

# Building
Install dependencies, dir into the clone folder and run 'make'. You also need automake deps that are not listed here.

```
$ doas pacman -Syyu
$ doas pacman -S sdl2 sdl2_gfx sdl2_image sdl2_mixer
$ cd dir/to/clone
$ ./configure
$ make
```
After it has compiled run:

```
$ ./chip8 dir/to/ROM
```

# Images 

![image](https://gitlab.com/pinkeee/chip8-rewritten/-/raw/main/image.png)
![image2](https://gitlab.com/pinkeee/chip8-rewritten/-/raw/main/image2.png)

# Video

[![Demo Video](https://streamable.com/kjha1z#)](https://streamable.com/kjha1z#)

# TODO

 - ̶A̶d̶d̶ ̶c̶o̶r̶r̶e̶c̶t̶ ̶i̶m̶p̶ ̶o̶f̶ ̶d̶e̶l̶a̶y̶ ̶t̶i̶m̶e̶r̶
 - Add cool GTK UI
 - ̶F̶i̶x̶ ̶s̶o̶u̶n̶d̶T̶i̶m̶e̶r̶/̶b̶u̶z̶z̶e̶r̶
 - ̶A̶d̶d̶ ̶d̶e̶b̶u̶g̶ ̶f̶l̶a̶g̶s̶
