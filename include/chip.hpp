#pragma once

#ifndef _CHIP_HPP_
#define _CHIP_HPP_

#include <fstream>
#include <iomanip>
#include <stdexcept>
#include <random>
#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include <chrono>
#include "SDL.hpp"

class Chip8 : public SDL
{
	public:
		Chip8(std::string); /* Constructor */
		~Chip8() = default; /* Deconstructor */

		int8_t delayTimer; /* CPU delay timer */
		int8_t soundTimer; /* Sound timer that controls when beeps should be played */
		
		unsigned char gfx[64][32]; /* Video buffer */
		unsigned char key[16]; /* Key press buffer */

		void initemu(void); /* Function to init emulator vars */
		void cycle(void); /* Main CPU cycle function */
		void clocktick(void); /* Increase things like delay and sound timer */
		void loadRom(std::string); /* Load ROM into correct place in memory */

		bool debugFlag; /* Set when the program is run, if so prints out things like current opcode */
	private:
		std::array<unsigned char, 0x1000> memory; /* Memory array with 4kb space */
		std::array<unsigned char, 16> V; /* CPU registers */
		std::array<int16_t, 16> stack; /* Stack with size of 16 */
		
		std::chrono::time_point<std::chrono::high_resolution_clock> timeAtWrite; /* Time at write of delayTimer */

		int16_t sp; /* Stack pointer */
		int16_t I; /* CPU index register */
		int16_t pc; /* CPU program counter */
};
#endif /* _CHIP_HPP_ */