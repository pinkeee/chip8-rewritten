#pragma once

#ifndef _SDL_HPP_
#define _SDL_HPP_

#define H 32
#define W 64
#define SCALE 8

#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>

class SDL
{
    public:
        SDL() = default; /* Default constructor */
        ~SDL(); /* Deconstructor */
        
        void initsdl(void); /* Init SDL vars + window */
        
        int waitkey(unsigned char[16]); /* While loop that waits for a keypress to continue */
        int key_press(unsigned char[16]); /* Set emu key var with correct key code data */

        void destroy(void); /* Used to remove all SDL stuff from memory */
		void render(unsigned char[64][32]); /* Render data in graphics buffer */
		void playbeep(uint8_t); /* Play sound when soundTimer is correct (called from chip.cpp) */

        bool quit; /* Quit var that is to tell the emu when to exit */
			
		SDL_Event event; /* SDL variable to store keypress events */
    private:
        SDL_Rect colourBuff = {0,0,8,8}; /* Colour buffer */

		SDL_Window* window = nullptr; /* SDL window var */
		SDL_Surface* surface = nullptr; /* SDL window surface var */
		Mix_Chunk* audioInterface = nullptr; /* SDL audio interface */

		int16_t rgbBuff[2]; /* RgbBuff, maps colour to pixels */

        unsigned char currentGfx[64][32]; /* What pixels are currently on the screen. We use this var to see if we need to render a pixel again */
};
#endif /* _SDL_HPP_ */